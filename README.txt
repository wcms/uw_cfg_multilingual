With 1.10  This module will turn off all language selectors on the node edit form pages.

With future releases this will be turn on always.  Language selectors will only be shown if there are multiple languages on a site.
